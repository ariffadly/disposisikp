<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suratmasuk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('md_suratmasuk');
		$this->load->model('md_pengguna');
		$this->load->helper('time_passed');
		$this->load->helper('encrypt');
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Jakarta');
	}


	public function halaman_suratmasuk(){
		
		/*Pengguna*/
		$data['suratmasuk']= $this->md_suratmasuk->getSuratMasukAll();		
		/* Hak AKses */
		//$this->load->model('md_hakakses');
		//$data['hak_akses']= $this->md_hakakses->getHakAkses();		
		$this->load->view('inputsuratmasuk',$data);
		/*print_r($hasil);*/
	}



	//BAGIAN SURAT
	public function halaman_inputsurat(){
	$data['jenis_user'] = $this->md_pengguna->getJenisUserAll();
	$this->load->view('inputsuratmasuk', $data);
	}

	public function tambah_surat(){
        $config['upload_path'] = './upload/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 5000;

        $this->load->library('upload', $config);

		$data_input=array();
		$data_input['pengirim']=$this->input->post('pengirim');
		$data_input['tanggal_surat'] = $this->input->post('tanggal_surat');
		$data_input['nomor_surat'] = $this->input->post('nomor_surat');
		$data_input['tanggal_diterima'] = $this->input->post('tanggal_diterima');
		$data_input['perihal_surat'] = $this->input->post('perihal_surat');
//		$data_input['file'] = $this->input->post('file');

        if ($this->upload->do_upload('file')) {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data_input['file'] = $file_name;
        }
	
		/*print_r($data_input);*/
		$data_ekspedisi = [];
		$data_ekspedisi['id_suratmasuk'] = $this->md_suratmasuk->insertsurat($data_input);
		$data_ekspedisi['id_pengguna'] = $this->session->userdata('nip');
		$data_ekspedisi['jenis_user_target'] = $this->input->post('jenis_user');
		$data_ekspedisi['jenis_user_pengirim'] = $this->session->userdata('jenis_user');
		$data_ekspedisi['tanggal'] = date("Y-m-d H:i:s");
		$this->md_suratmasuk->insertEkspedisiSurat($data_ekspedisi);

		redirect(base_url().'suratmasuk/halaman_listsurat');
	}
	public function halaman_listsurat(){
		$data['pengguna'] = $this->md_suratmasuk->getSuratMasukAll($this->session->userdata('jenis_user'));
		$data['jenis_user'] = $this->md_pengguna->getJenisUserAll(); 
		$this->load->view('listsurat',$data);
	}

	public function halaman_lihatsurat(){
		$id_surat=$this->uri->segment(3);
		$data['pengguna']=$this->md_suratmasuk->getSuratMasuk($id_surat);
		$this->load->view('lihatsurat',$data);
	}
		public function lihat_suratbagian(){
		$id_surat=$this->uri->segment(3);
		$data['pengguna']=$this->md_suratmasuk->getSuratMasuk($id_surat);
		$this->load->view('lihat_suratbagian',$data);
	}
	public function halaman_bagian(){
		$data['pengguna']=$this->md_suratmasuk->getSuratMasukAll();
		$this->load->view('listsuratbagian',$data);
	}
		public function halaman_disposisibagian(){
		$data['pengguna']=$this->md_suratmasuk->getSuratMasukAll();
		$this->load->view('listdisposisibagian',$data);
	}
		public function halaman_profile(){
		$data['pengguna']=$this->md_pengguna->getPenggunaAll();
		$this->load->view('lihatprofile',$data);
	}
		public function halaman_profilebagian(){
		$data['pengguna']=$this->md_pengguna->getPenggunaAll();
		$this->load->view('lihatprofilebagian',$data);
	}

		public function teruskan_surat() {
			foreach($this->input->post('jenis_user') as $selected){
				$data_ekspedisi = [];
				$data_ekspedisi['id_suratmasuk'] = $this->input->post('id_surat');
				$data_ekspedisi['id_pengguna'] = $this->session->userdata('nip');
				$data_ekspedisi['jenis_user_target'] = $selected;
				$data_ekspedisi['jenis_user_pengirim'] = $this->session->userdata('jenis_user');
				$data_ekspedisi['tanggal'] = date("Y-m-d H:i:s");
				$data_ekspedisi['jenis_target'] = $this->md_suratmasuk->getJenisUser($selected);
                $this->md_suratmasuk->insertEkspedisiSurat($data_ekspedisi);
			}

			if($this->input->post('perintah')) {
				$this->md_suratmasuk->updatePerintah($this->input->post('perintah'));
			}

			redirect (site_url('suratmasuk/halaman_listsurat'));
		}

		public function ekspedisiSurat() {
		    $data = [];
		    $data['ekspedisi'] = $this->md_suratmasuk->getEkspedisi($this->uri->segment(3));
            $this->load->view('ekspedisi_surat', $data);
        }

	//selesai bagian surat


}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */