<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('md_pengguna');
		$this->load->helper('time_passed');
		$this->load->helper('encrypt');
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		if ($this->session->userdata('nip')) {
			redirect(base_url() . 'suratmasuk/halaman_listsurat', 'refresh');
		}
	   

		$this->load->view('login');
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url().'pengguna');
	}
	public function dashboard()
	{
		if (!$this->session->userdata('nip'))
			redirect(base_url() . 'pengguna', 'refresh');
		$this->load->view('berandaadmin');
	}

	public function HalamanEditAdmin(){
		$data['pengguna']=$this->md_pengguna->getProfileAll();
		$this->load->view('editAdmin',$data);
	}
	
	public function profile(){
		$data['pengguna']=$this->md_pengguna->getProfileAll();
		$this->load->view('profile',$data);
	}
	public function profileadmin(){
		$data['pengguna']=$this->md_pengguna->getProfileAll();
		$this->load->view('profileadmin',$data);
	}


	public function cek_login(){
		$data = array('nip'=>$this->input->post('nip'),
			'password'=>$this->input->post('password')
			);

		$hasil= $this->md_pengguna->getPenggunaByNIP($this->input->post('nip'));
		//print_r($hasil);
		
		if($hasil)
		{
		    echo "masuk2";
			$pass=$hasil[0]->password;
			if($pass==$this->input->post('password'))
			{
				foreach($hasil as $sess){
					$sess_data['nip']=$sess->nip;
					$sess_data['jenis_user']=$sess->jenis_user;
					$sess_data['namaa']=$sess->nama;
					$sess_data['password']=$sess->password;
					$sess_data['golongan']=$sess->golongan;
					$this->session->set_userdata($sess_data);
					redirect('suratmasuk/halaman_listsurat');
				}
			}
				else{
		    	$msg="Username dan Password anda tidak sama";
				$this->session->set_flashdata('err_login',$msg);
				redirect(base_url().'pengguna');
			}

	}}

	public function halaman_pengguna(){
		
		/*Pengguna*/
		$data['pengguna']= $this->md_pengguna->getPenggunaAll();		
		/* Hak AKses */
		//$this->load->model('md_hakakses');
		//$data['hak_akses']= $this->md_hakakses->getHakAkses();		
		$this->load->view('pengguna',$data);
		/*print_r($hasil);*/
	}


	public function tambah_pengguna(){
		$data_input=array();
		$data_input['nip']=$this->input->post('nip');
		$data_input['nama'] = $this->input->post('nama');
		$data_input['password'] = $this->input->post('password');
		$data_input['golongan'] = $this->input->post('golongan');
		$data_input['jenis_user'] = $this->input->post('jenis_user');
	
		/*print_r($data_input);*/
		$this->md_pengguna->insertPengguna($data_input);
		redirect(base_url().'pengguna/halaman_listpengguna');
	}
	public function halaman_listpengguna(){
		$data['pengguna']=$this->md_pengguna->getPenggunaAll();
		$this->load->view('listPengguna',$data);
	}
	public function editpengguna2(){
	    $data_input=array();
	    $nip = $this->input->post('nip');
		$data_input['nama'] = $this->input->post('nama');
		$data_input['golongan'] = $this->input->post('golongan');
		$data_input['jenis_user'] = $this->input->post('jenis_user');
		/*print_r($data_input);*/
		if($this->md_pengguna->updatePengguna($nip, $data_input))
		redirect(base_url().'pengguna/halaman_listpengguna');
	}
	public function editPengguna(){
		$id=$this->input->get('id');
		$data['pengguna']=$this->md_pengguna->getPenggunaByNip($id);
		$this->load->view('editpengguna',$data);
	}

	public function hapus_pengguna(){
		$id=$this->input->get('id');
		$this->md_pengguna->deletePengguna($id);
		redirect(base_url().'pengguna/halaman_listpengguna');
	}

	public function halaman_registrasi(){
		$data['pengguna']=$this->md_pengguna->getPenggunaAll();
		$this->load->view('tambahpengguna',$data);
	}
    public function edit_profile(){
        $data_input=array();
        $id=$this->input->get('nip');
        $data_input['nip']=$this->input->get('nip');
        $data_input['nama'] = $this->input->get('nama');
        $data_input['password'] = $this->input->get('password');
        $this->session->set_userdata($data_input);
        /*print_r($data_input);*/
        $this->md_pengguna->updatePengguna($id,$data_input);
        redirect(base_url().'pengguna');
    }
    public function halaman_profile(){
        $this->load->view('profile');
    }

}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */