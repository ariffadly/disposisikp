<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suratkeluar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('md_suratkeluar');
        $this->load->model('md_pengguna');
        $this->load->helper('time_passed');
        $this->load->helper('encrypt');
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function halaman_nosurat(){
        $data['pengguna']=$this->md_suratkeluar->getNoSuratKeluarAll();
        $this->load->view('list_nosurat',$data);
    }

    public function halaman_inputsurat(){
        $data['jenis_user'] = $this->md_pengguna->getJenisUserAll();
        $data['kode_surat'] = $this->md_suratkeluar->getNoSuratKeluarAll();
        $data['last_id'] = $this->md_suratkeluar->getLastId();
        $this->load->view('inputsuratkeluar', $data);
    }

    public function tambah_surat(){

        $data_input=array();
        $data_input['id']=$this->input->post('id');
        $data_input['no_surat'] = $this->input->post('no_surat');
        $data_input['nama_request'] = $this->input->post('nama');
        $data_input['id_jabatan_request'] = $this->input->post('jenis_user');
        $data_input['keterangan'] = $this->input->post('keterangan');
        $data_input['tanggal'] = $this->input->post('tanggal');
        $this->md_suratkeluar->insertsurat($data_input);

        redirect(base_url().'suratkeluar/halaman_listsurat');
    }

    public function halaman_listsurat(){
        $data['suratkeluar'] = $this->md_suratkeluar->getSuratKeluarAll();
        $this->load->view('listsuratkeluar',$data);
    }
}