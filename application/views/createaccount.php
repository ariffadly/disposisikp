<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>

</head>

<body>

<!-- END RIGHTBAR -->
<div id="content-header">
   <div id="content-header">
    <div id="breadcrumb"> <a href="<?php echo base_url()."pengguna"; ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Back </a></div>
  </div>
  </div>
  <div id="content">
  <div class="container-fluid">
       <div class="row-fluid">
    <div class="span8">
      <div class="widget-box2">
        <div class="widget-title"> 
          <h5>Create an Account</h5>
        </div>
        <form action="<?php echo base_url().'pengguna/tambah_pengguna'; ?>" method='POST' class="form-horizontal">
          <div class="control-group2">
              <label class="control-label">Full Name</label>
              <div class="controls">
                <input type="text" class="span4" placeholder="Full Name" id="nama" name="nama" required/>
              </div>
            </div>
          <div class="control-group2">
              <label class="control-label">Username</label>
              <div class="controls">
                <input type="text" class="span4" placeholder="Username" id="username" name="username" required/>
              </div>
            </div>
            <div class="control-group2">
              <label class="control-label">Password</label>
              <div class="controls">
                <input type="password" class="span4" placeholder="Password" id="password" name="password" required/>
              </div>
            </div>
            
            <div class="control-group2">
              <label class="control-label">Email</label>
              <div class="controls">
                <input type="text" class="span4" placeholder="Email (optional)" id="email" name="email" required />
              </div>
            </div>
            <div class="control-group2">
              <label class="control-label">Address</label>
              <div class="controls">
                <input type="text" class="span4" placeholder="Address" id="alamat" name="alamat" />
              </div>
            </div>
            <div class="control-group2">
              <label class="control-label">Jenis User</label>
              <div class="controls">
                <select name="jenis_user" id="jenis_user">
                  <option value="1">Mahasiswa</option>
                  <option value="2">Dosen</option>
                </select>
              </div>
            </div>
            <div class="control-group">
              <div class="control-label">
                <img id="blah" width="100px" height="100px" alt="User" src="<?php echo site_url('assets/img2/demo/av2.jpg');?>">
              </div>
              <div class="controls">
                <input type="file" id="foto" name="foto" onchange="readURL(this);"/>
              </div>
            </div>
            <div class="form-actions">
              <center><button type="submit" class="btn btn-info btn-submit" title="Save">Save</button></center>
            </div>
          </form>
        </div>
      </div>
  </div>
</div></div>
</div>
</div>
  <!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; E-Learning</div>
</div>
<!--end-Footer-part-->

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
<script>
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	</script>

</body>
</html>