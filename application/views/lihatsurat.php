<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebaruser');
    ?>
  <div id="content">
  <div class="container-fluid">
  <h2>Lihat Surat Masuk</h2>
       <div class="row-fluid">
       <div class="span12">
      <div class="widget-box2">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Halaman Lihat Surat</h5>
        </div>
        
           <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
      <div class="widget-box2">
                <div class="widget-content">
                <?php foreach ($pengguna as $data_surat){ ?>
                <input type="hidden" name="id" value="<?= $data_surat->id_surat;?>">
            <div class="row-fluid">
              <div class="span6">
                <table class="table table-bordered table-invoice-full">
                  <tbody>
                    <tr>
                      <td><h4>Detail Surat</h4></td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Surat Dari</label></td>
                    <td>
                    <input type="text" name="pengirim" id="pengirim" class="span11" disabled="" placeholder="Pengirim Surat" value="<?= $data_surat->pengirim;?>" />
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Tanggal Surat</label></td>
                    <td>
                    <input type="text" class="span11" name="tanggal_surat" id="tanggal_surat" disabled="" placeholder="Tanggal Surat" value="<?= $data_surat->tanggal_surat;?>" />
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Nomor Surat</label></td>
                    <td>
                    <input type="text" class="span11" name="nomor_surat" id="nomor_surat" disabled="" placeholder="Nomor Surat" value="<?= $data_surat->nomor_surat;?>"/>
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Tanggal Diterima</label></td>
                    <td>
                    <input type="text" class="span11" name="tanggal_diterima" id="tanggal_diterima" disabled="" placeholder="Tanggal Diterima" value="<?= $data_surat->tanggal_diterima;?>" />
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Perihal Surat</label></td>
                    <td>
                    <input type="text" class="span11" placeholder="Perihal Surat" disabled="" name="perihal_surat" id="perihal_surat" value="<?= $data_surat->perihal_surat;?>" />
                    </td>
                    </tr>
                  </tbody>
                </table>
                        
              </div>
              <div class="span6">
              <div class="widget-box2">
                <div class="widget-content">
            <div class="row-fluid">
                <h4>Please click the icon below to view this file</h4>
                <a href="<?= site_url('upload/'.$data_surat->file)?>" target="_blank">
                    <img src="<?= site_url('assets/PDF.png') ?>" width="150" height="150">
                    <h5 style="margin-top: -10px; margin-left: 1px;"><?= $data_surat->file ?></h5>
                </a>
              </div><br>
                  </div>
            </div>
          </div>

          </div>
          <?php }?>
          </div>
          </div>
        </div>
      </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>

<!--end-Footer-part-->



<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
