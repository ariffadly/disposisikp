<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebaruser');
    ?>
  <div id="content">
  <div class="container-fluid">
  <h2>Ubah Data Diri</h2>
       <div class="row-fluid">
       <div class="span10">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Halaman Ubah Data Diri</h5>
        </div>
      <form id="form-data" method="post" action="<?php echo base_url().'pengguna/edit_profile'; ?>">
      <table align="center">
      <tr>
      </div>
      <td>
            <div class="article-post">
            <form action="#" method="get" class="form-horizontal">
            <div class="control-group">
              <label class="control-label"><b>Nama Lengkap</b></label>
              <div class="controls">
                <input type="text" name="nama" id="uname" class="span12" placeholder="Nama Lengkap" value="">

            </div>
            <div class="control-group">
              <label class="control-label"><b>Kata Sandi</b><label>
              <div class="controls">
                <input type="text" name="password" id="pass" class="span12" placeholder="Kata Sandi" value="">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><b>Nomor Induk Pegawai</b></label>
              <div class="controls">
                <input type="text" name="alamat" id="almt" class="span12" disabled="" placeholder="Nomor Induk Pegawai" value="">
              </div>
            </div>
          </form>
          </div>
          <br>
      <table>
      <tr>
      <td>
      <button class="tombol" data-wysihtml5-command="insertImage" title="Cancel" href="javascript:;" unselectable="on">Batal</button>
      </td>
      <td>
        <button class="tombol btn-info btn-submit" title="Save">Simpan</button>
      </td>
      </div>
      </td>
      </tr>
      </table>
      </td>
      </tr>
      </table>
      </form>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>

<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>
<!--end-Footer-part-->

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
<!--
<script>
document.getElementById('hapus').onClick=function(){
  alert('Yakin Dihapus?');
    window.location="<?php echo site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->id_pengguna) ?>";

};
</script>
-->
</body>
</html>