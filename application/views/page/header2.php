<header id="header">
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Selamat Datang User</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="<?php echo base_url()."pengguna/halaman_profilebagian"; ?>"><i class="icon-user"></i> Data Diri Pribadi</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="<?= site_url('pengguna/logout') ?>"><i class="icon icon-share-alt"></i> <span class="text">Keluar</span></a></li>
  </ul>
</div>

<!--start-top-serch-->
<div id="search">
  <input type="text" placeholder="Cari disini...."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch--> 
</header>