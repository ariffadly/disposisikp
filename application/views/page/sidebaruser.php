<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <table>
        <tr>
            <td>
                <img src="<?=base_url();?>assets/img/logo-sidebar.png" alt="" style="margin-left: 5px; !important;"/>
            </td>
            <td>
                <h5 style="margin-left: 10px; !important;">Badan Perencanaan Pembangunan Daerah Kota Pekanbaru</h5>
            </td>
        </tr>
    </table>
  <ul>
    <!-- <li class=""><a href="<?= base_url()."pengguna"; ?>"><i class="icon icon-home"></i> <span>Beranda</span></a> </li> -->
      <?php if($this->session->userdata('jenis_user') == 2) { ?>
      <li class="submenu"> <a href="#"><i class="icon icon-th"></i> <span>Kelola Pengguna</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="<?= base_url()."pengguna/halaman_registrasi"; ?>">Tambah Pengguna</a></li>
        <li><a href="<?= base_url()."pengguna/halaman_listPengguna"; ?>">Daftar Pengguna</a></li>
      </ul>
    </li>
      <li class="submenu"> <a href="#"><i class="icon-envelope-alt"></i> <span>Surat Masuk</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="<?= base_url()."suratmasuk/halaman_inputsurat"; ?>">Masukkan Surat</a></li>
        <li><a href="<?= base_url()."suratmasuk/halaman_listsurat"; ?>">Daftar Surat Masuk</a></li>
        </ul>
    </li>
    <?php } else { ?>
    <li class=""><a href="<?= base_url()."suratmasuk/halaman_listsurat"; ?>"><i class="icon icon-envelope-alt"></i> <span>Surat Masuk</span></a> </li>
    <?php } if($this->session->userdata('jenis_user') == 2) { ?>
    <li class="submenu"> <a href="#"><i class="icon-envelope"></i> <span>Surat Keluar</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="<?= site_url('suratkeluar/halaman_inputsurat') ?>">Buat Surat</a></li>
        <li><a href="<?= site_url('suratkeluar/halaman_listsurat') ?>">Daftar Surat Keluar</a></li>
          <li><a href="<?= base_url()."suratkeluar/halaman_nosurat"; ?>">Daftar Nomor Surat</a></li>
      </ul>
    </li>
      <?php } ?>
    <!-- <li><a href="#"><i class="icon-globe"></i> <span>Lacak Surat</span></a> </li> -->
  </ul>

</div>
