<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/header2');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebarbagian');
    ?>
<!-- START -->
  <div id="content">
  <div class="container-fluid">
  <h2>Surat Masuk</h2>
        <div class="widget-box2">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Daftar Surat Masuk</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Tanggal Masuk</th>
                  <th>Tanggal Surat</th>
                  <th>Nomor Surat</th>
                  <th>Pengirim</th>
                  <th>Perihal</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody align="center">
              <?php $i=0;foreach ($pengguna as $data_surat) {$i++;?>
                <tr class="odd gradeX">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $data_surat->tanggal_diterima; ?></td>
                  <td><?php echo $data_surat->tanggal_surat; ?></td>
                  <td><?php echo $data_surat->nomor_surat; ?></td>
                  <td><?php echo $data_surat->pengirim; ?></td>
                  <td><?php echo $data_surat->perihal_surat; ?></td>
                  

                   <td class="center">
                    <a
                    data-tglterima="<?php echo $data_surat->tanggal_diterima; ?>"
                    data-tglsurat="<?php echo $data_surat->tanggal_surat; ?>"
                    data-nmrsurat="<?php echo $data_surat->nomor_surat; ?>"
                    data-pengirim="<?php echo $data_surat->pengirim; ?>"
                    data-perihalsurat="<?php echo $data_surat->perihal_surat; ?>">
                    
                    <center><a href="<?php echo base_url()."pengguna/lihat_suratbagian/".$data_surat->id_surat; ?>" class="btn btn-mini">Lihat Surat</a> 
                  <!--<a href="#" class="btn btn-primary btn-mini">Disposisi</a> -->
                    <a href="#" class="btn btn-success btn-mini">Ekspedisi Surat</a></td>
                    </center>
                </tr>
                  <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
</div>
      
        </div>
      </div>
    </div>
  </div>
</div>

<!--end-main-container-part-->


<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>

<!--end-Footer-part-->



<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
