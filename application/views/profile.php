<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('page/title');
?>
<head>
    <meta charset="utf-8">

    <?php
    $this->load->view('page/meta_css');
    $this->load->view('page/js');

    ?>
</head>


<body>
<?php
$this->load->view('page/headeradmin');
?>
<!-- END RIGHTBAR -->

<?php
$this->load->view('page/sidebaruser');
?>
<!-- START -->
<div id="content">
    <div class="container-fluid">
        <h2>Profil</h2>
        <div class="row-fluid">
            <div class="span9">
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2">
                        <h5>Ubah Data Diri</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form action="<?= base_url().'pengguna/edit_profile'; ?>" method="get" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" style="margin-left: 22px; !important;">NIP</label>
                                <div class="controls">
                                    <input type="text" name="nip" id="nip" style="margin-left: 115px; !important;" placeholder="Nomor Induk Pegawai" value="<?= $this->session->userdata('nip')?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="margin-left: 93px; !important;">Nama Lengkap</label>
                                <div class="controls">
                                    <input type="text" name="nama" id="nama" style="margin-left: 43px; !important;" placeholder="Nama Lengkap" value="<?= $this->session->userdata('namaa')?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="margin-left: 55px; !important;">Password</label>
                                <div class="controls">
                                    <input type="text"  name="password" id="password" style="margin-left: 80px; !important;" placeholder="Password" value="<?= $this->session->userdata('password')?>">
                                </div>
                            </div>
                            <div class="text-center" style="margin-top: 10px; !important;">
                                            <a class="tombol btn btn-default" title="Cancel" onclick="history.back()">Batal</a>
                                            <button class="tombol btn btn-info btn-submit" title="Save">Simpan</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Footer-part-->
<?php
$this->load->view('page/footer');
?>
<!--end-Footer-part-->

<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>
<!--
<script>
document.getElementById('hapus').onClick=function(){
  alert('Yakin Dihapus?');
    window.location="<?= site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->id_pengguna) ?>";

};
</script>
-->
</body>
</html>