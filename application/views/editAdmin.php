<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
	<meta charset="utf-8">
	
	<?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>

</head>
<?php
  //$this->load->view('page/headerbar');
?>

<?php
  $this->load->view('page/headerAdmin');
  ?>

    
<div id="content">
  <div class="container-fluid">
  <div class="row-fluid">
    <div class="span5">
    <div class="widget-box2">
     <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2">
        <h5>Edit My Profile</h5>
      </div>
        <div class="widget-content nopadding">
          <form action="<?php echo base_url().'pengguna/editAdmin2'; ?>" method="get" class="form-horizontal">
          <?php foreach ($pengguna as $data_pengguna){ ?>
            <input type="hidden" name="id" value="<?php echo $data_pengguna->nip;?>">
            <div class="control-group">
              <label class="control-label">Full Name</label>
              <div class="controls">
                <input type="text" name="nama" id="nama" class="span6" placeholder="Full Name" value="<?php echo $data_pengguna->nama;?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Username</label>
              <div class="controls">
                <input type="text" name="username" id="username" class="span6" placeholder="Username" value="<?php echo $data_pengguna->username;?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Address</label>
              <div class="controls">
                <input type="text"  name="alamat" id="alamat" class="span6" placeholder="Address" value="<?php echo $data_pengguna->alamat;?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email</label>
              <div class="controls">
                <input type="text"  name="email" id="email" class="span6" placeholder="Email" value="<?php echo $data_pengguna->email;?>">
              </div>
            </div>
            <table align="center">
      <tr>
      <td>
      <button class="tombol" data-wysihtml5-command="insertImage" title="Cancel" href="javascript:;" unselectable="on">Cancel</button>
      </td>
      <td>
        <button class="tombol btn-info btn-submit" title="Save">Save Changes</button>
      </td>
      </tr>
      </table>
        <?php }
        ?>
      </form>
      </div>
      </td>
      </tr>
      </table>
          </form>
        </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
  <!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; E-Learning</div>
</div>
<!--end-Footer-part-->

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>

</body>
</html>