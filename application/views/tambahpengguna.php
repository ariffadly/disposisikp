<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>




<body>

<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebaruser');
    ?>
<!-- START -->
  <div id="content">
  <div class="container-fluid">
  <h2>Tambah Pengguna</h2>
       <div class="row-fluid">
    <div class="span9">
    <br>
      <div class="widget-box">
        <div class="widget-title"> 
          <h5>Formulir Tambah Pengguna</h5>
        </div>
        <form action="<?php echo base_url().'pengguna/tambah_pengguna'; ?>" method='POST' class="form-horizontal">
          <div class="control-group2">
              <label class="control-label" style="margin-left: 40px; !important;">NIP</label>
              <div class="controls">
                <input type="text" style="margin-left: 89px; !important;" placeholder="Nomor Induk Pegawai" id="nip" name="nip" required/>
              </div>
            </div>
          <div class="control-group2">
              <label class="control-label" style="margin-left: 82px; !important;">Kata Sandi</label>
              <div class="controls">
                <input type="password" style="margin-left: 46px; !important;" placeholder="Kata Sandi" id="password" name="password" required/>
              </div>
            </div>
            <div class="control-group2">
              <label class="control-label" style="margin-left: 109px; !important;">Nama Lengkap</label>
              <div class="controls">
                <input type="text" style="margin-left: 20px; !important;" placeholder="Nama Lengkap" id="nama" name="nama" required/>
              </div>
            </div>
            
            <div class="control-group2">
              <label class="control-label" style="margin-left: 73px; !important;">Golongan</label>
              <div class="controls">
                <input type="text" style="margin-left: 56px; !important;" placeholder="Golongan" id="golongan" name="golongan" required />
              </div>
            </div>
            <div class="control-group2">
              <label class="control-label" style="margin-left: 75px; !important;">Jenis User</label>
              <div class="controls">
                <select name="jenis_user" id="jenis_user" style="margin-left: 53px; !important;">
                  <option value="1">Kepala BAPPEDA</option>
                  <option value="2">Admin Surat</option>
                  <option value="3">Sekretaris BAPPEDA</option>
                  <option value="4">Kasubbag Umum</option>
                  <option value="5">Bidang Keuangan</option>
                  <option value="6">Bidang Pengembangan Permukiman dan Prasarana Wilayah (P3W)</option>
                  <option value="7">Bidang Kesejahteraan Sosial dan Sumber Daya (KS2D)</option>
                  <option value="8">Bidang Perekonomian dan Pemerintahan (PP)</option>
                  <option value="9">Bagian Sekretariat</option>
                </select>
              </div>
            </div>
            <div>
              <center><button type="submit" class="btn btn-success btn-submit" style="margin-top: 20px; !important;" title="Simpan">Simpan</button></center>
            </div>
          </form>
        </div>
      </div>
  </div>
</div>
</div>
</div>
<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>
<!--end-Footer-part-->

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
<!--
<script>
document.getElementById('hapus').onClick=function(){
  alert('Yakin Dihapus?');
    window.location="<?php echo site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->id_pengguna) ?>";

};
</script>
-->
</body>
</html>