<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Disposisi Surat</title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Bapedda">
	<meta name="author" content="Vanya Anjani Mupti">

	<!-- <link href="assets/less/styles.less" rel="stylesheet/less" media="all"> -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css?=140">
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
	
</head><body class="focusedform">

<div class="verticalcenter">
    <center><img src="assets/img/logo-pekanbaru.png" alt="demo-image"/>
        <h3>Panel Login Disposisi Surat<br>BAPPEDA Kota Pekanbaru </h3></center>

	<div class="panel panel-primary">
		<div class="panel-body">
			<h4 class="text-center" style="margin-bottom: 25px;">Silahkan Masuk </h4>
			<form action="<?php echo base_url().'pengguna/cek_login'; ?>" method='POST' class="form-horizontal">
				<div class="form-group">
					<label for="email" class="control-label col-sm-4" style="text-align: left;">NIP</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="nip" name="nip" placeholder="NIP">
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="control-label col-sm-4" style="text-align: left;">Kata Sandi</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" id="password" name="password" placeholder="Kata Sandi">
					</div>
				</div>
				<button class="btn btn-primary btn-block" type="submit">Masuk</button>
			</form>
			<br><br>
			<?php if($this->session->flashdata()){
				?>
				<div class="alert alert-dismissable alert-danger">
					<?php echo $this->session->flashdata('err_login'); ?>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				</div>
				<?php
			}?>
			
		</div>
		<!-- <div class="panel-footer">
			<a href="extras-forgotpassword.htm" title="Help you If you lost&#013;your password" class="pull-left btn btn-link" style="padding-left:0">Forgot password?</a>
			<a href="<?php echo base_url(); ?>pengguna/halaman_registrasi" title="Create your new account If you&#013;haven't an account" class="pull-left btn btn-link" style="padding-left:0">Create an Account</a>
			<div class="pull-right">
				<a href="#" title="Remove all of item already written" class="btn btn-default">Reset</a>
			</div>
		</div> -->
	</div>
</div>


</body>
</html>