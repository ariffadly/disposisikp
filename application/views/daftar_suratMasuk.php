<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebaruser');
    ?>
<!-- START -->
  <div id="content">
  <div class="container-fluid">
  <h2>Daftar Pengguna</h2>
       <div class="row-fluid">
    <div class="span12">
      <div class="widget-content">
        <div class="control-group">
          <div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Nama Lengkap</th>
                  <th>NIP</th>
                  <th>Golongan</th>
                  <th>Jenis User</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0;foreach ($pengguna as $data_pengguna) {$i++;?>
                <tr class="odd gradeX">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $data_pengguna->nama; ?></td>
                  <td class="center"> <?php echo $data_pengguna->nip; ?></td>
                  <td class="center"> <?php echo $data_pengguna->golongan; ?></td>
             <td class="center"> 
            <?php
            if ($data_pengguna->jenis_user == 1) {
              echo "Kepala BAPPEDA"; 
            } 
            else if ($data_pengguna->jenis_user == 2) {
              echo "Admin Surat";
            }
            else if ($data_pengguna->jenis_user == 3) {
              echo "Sekretaris BAPPEDA";
            }
            else if ($data_pengguna->jenis_user == 4) {
             echo "Kassubag Umum";
            }
            else if ($data_pengguna->jenis_user == 5) {
              echo "Bidang Keuangan";
            }
            else if ($data_pengguna->jenis_user == 6){
              echo "Bidang P3W";
            }
            else if ($data_pengguna->jenis_user == 7) {
              echo "Bidang KS2D";
            }
            else if ($data_pengguna->jenis_user == 8) {
              echo "Bidang PP";
            }
            else if ($data_pengguna->jenis_user == 9) {
              echo "Sekretariat";
            }
            else{
              echo "Kepala BAPPEDA";
            }
            ?>
           </td>
                  
                  <td class="center">
                    <a
                    data-nip="<?php echo $data_pengguna->nip; ?>"
                    data-nama="<?php echo $data_pengguna->nama; ?>"
                    data-golongan="<?php echo $data_pengguna->golongan; ?>"
                    data-jenis_user="<?php echo $data_pengguna->jenis_user; ?>"
                    <td class="taskOptions"><a href="<?php echo site_url('pengguna/editPengguna?id=' .$data_pengguna->nip) ?>" class="tip-top"><i class="icon-edit"></i></a> <a href="<?php echo site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->nip) ?>" class="tip-top" ><i class="icon-remove"></a></i></td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          </div>
</div>
</div>
</div>
</a>
</a>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>
<!--end-Footer-part-->

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
<!--
<script>
document.getElementById('hapus').onClick=function(){
  alert('Yakin Dihapus?');
    window.location="<?php echo site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->id_pengguna) ?>";

};
</script>
-->
</body>
</html>