<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('page/title');
?>
<head>
    <meta charset="utf-8">

    <?php
    $this->load->view('page/meta_css');
    $this->load->view('page/js');

    ?>
</head>


<body>
<?php
$this->load->view('page/headeradmin');
?>
<!-- END RIGHTBAR -->

<?php
$this->load->view('page/sidebaruser');
?>
<!-- START -->
<div id="content">
    <div class="container-fluid">
        <h2>Daftar Surat Masuk</h2>
        <div class="widget-box2">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                <h5>Daftar Surat Masuk</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered data-table">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Nomor Surat</th>
                        <th>Jenis Pemohon Surat</th>
                        <th>Nama Pemohon</th>
                        <th>Keterangan</th>
                    </tr>
                    </thead>
                    <tbody align="center">
                    <?php $i=0;foreach ($suratkeluar as $data_surat) {$i++;?>
                        <tr class="odd gradeX">
                            <td><?= $data_surat->tanggal; ?></td>
                            <td><?= $data_surat->no_surat; ?></td>
                            <td><?= $data_surat->nama; ?></td>
                            <td><?= $data_surat->nama_request; ?></td>
                            <td><?= $data_surat->keterangan; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!--end-main-container-part-->


<!--Footer-part-->
<?php
$this->load->view('page/footer');
?>

<!--end-Footer-part-->


</body>

</html>
