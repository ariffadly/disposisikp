<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
	<meta charset="utf-8">
	
	<?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>

</head>
<?php
  //$this->load->view('page/headerbar');
?>

<?php
  $this->load->view('page/headeradmin');
  ?>

    
<body>
<!-- END RIGHTBAR -->
<div id="content">
  <div class="container-fluid">

    <div class="row-fluid">
<div id="content">
  <div id="content-header"></div>
  <div class="container-fluid">

  <div class="row-fluid">
    <div class="span12">
    <div class="widget-box">
     <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2">
        <h5>Edit My Profile</h5>
      </div>
      <form id="form-data" method="post" action="<?php echo base_url().'pengguna/edit_profile'; ?>">
      <table align="center">
      <tr>
      </div>
      <td>
            <div class="article-post">
            <form action="#" method="get" class="form-horizontal">
            <div class="control-group">
              <label class="control-label"><b>Your Name</b></label>
              <div class="controls">
                <input type="text" name="nama" id="uname" class="span12" placeholder="Your Name" value="<?php echo $this->session->userdata('namaa')?>">

            </div>
            </div>
            <div class="control-group">
              <label class="control-label"><b>Your Password</b><label>
              <div class="controls">
                <input type="text" name="password" id="pass" class="span12" placeholder="Your Password" value="<?php echo $this->session->userdata('password')?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"><b>Your Address</b></label>
              <div class="controls">
                <input type="text" name="alamat" id="almt" class="span12" placeholder="Your Address" value="<?php echo $this->session->userdata('alamat')?>">
              </div>
            </div>
            
            <table>
            <div class="control-group">
              <label class="control-label"><b>Profile Photo</b></label>
              <tr>
              <td>
                <img width="100px" height="100px" alt="User" src="<?php echo site_url('assets/img2/demo/av2.jpg');?>">
              </td>
              <td>
                <input type="file" name="filee" id="upload" style="display:none"/>
                <button class="tombol" data-wysihtml5-command="createLink" title="Edit Photo" href="javascript:;" unselectable="on" onclick="document.getElementById('upload').click();"><i class="icon-pencil">Upload Photo</i></button>
                <h6><p class="p">Maximum Size 1 MB .JPG, GIF, PNG</p></h6>
              </td>
              </tr>
              </div>
            </div>
            </table>
          </form>
          </div>
      <table>
      <tr>
      <td>
      <button class="tombol" data-wysihtml5-command="insertImage" title="Cancel" href="javascript:;" unselectable="on">Cancel</button>
      </td>
      <td>
        <button class="tombol btn-info btn-submit" title="Save">Save Changes</button>
      </td>
      </div>
      </td>
      </tr>
      </table>
      </td>
      </tr>
      </table>
      </form>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>

  <!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; E-Learning</div>
</div>
<!--end-Footer-part-->

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>

</body>
</html>