<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebarbagian');
    ?>
  <div id="content">
  <div class="container-fluid">
  <h2>Lihat Surat Masuk</h2>
       <div class="row-fluid">
       <div class="span12">
      <div class="widget-box2">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Halaman Lihat Surat</h5>
        </div>
        
           <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
      <div class="widget-box2">
                <div class="widget-content">
                <?php foreach ($pengguna as $data_surat){ ?>
                <input type="hidden" name="id" value="<?php echo $data_surat->id_surat;?>">
            <div class="row-fluid">
              <div class="span6">
                <table class="table table-bordered table-invoice-full">
                  <tbody>
                    <tr>
                      <td><h4>Detail Surat</h4></td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Surat Dari</label></td>
                    <td>
                    <input type="text" name="pengirim" id="pengirim" class="span11" disabled="" placeholder="Pengirim Surat" value="<?php echo $data_surat->pengirim;?>" />
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Tanggal Surat</label></td>
                    <td>
                    <input type="text" class="span11" name="tanggal_surat" id="tanggal_surat" disabled="" placeholder="Tanggal Surat" value="<?php echo $data_surat->tanggal_surat;?>" />
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Nomor Surat</label></td>
                    <td>
                    <input type="text" class="span11" name="nomor_surat" id="nomor_surat" disabled="" placeholder="Nomor Surat" value="<?php echo $data_surat->nomor_surat;?>"/>
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Tanggal Diterima</label></td>
                    <td>
                    <input type="text" class="span11" name="tanggal_diterima" id="tanggal_diterima" disabled="" placeholder="Tanggal Diterima" value="<?php echo $data_surat->tanggal_diterima;?>" />
                    </td>
                    </tr>
                    <tr>
                    <td>
                    <label class="control-label">Perihal Surat</label></td>
                    <td>
                    <input type="text" class="span11" placeholder="Perihal Surat" disabled="" name="perihal_surat" id="perihal_surat" value="<?php echo $data_surat->perihal_surat;?>" />
                    </td>
                    </tr>
                  </tbody>
                </table>
                        
              </div>
              <div class="span6">
              <div class="widget-box2">
                <div class="widget-content">
            <div class="row-fluid">
              Surat Ditampilin Disini
              </div><br>
              <div class="row-fluid">
                  <a href="lihatsurat.php" class="btn"><i class="icon-print">  Cetak</i></a> 
                  <a href="#" class="btn btn-primary"><i class="icon-download-alt"> Unduh</i></a>
                  </div>
                  </div>
            </div>
          </div>

          </div>
            <div class="row-fluid">
    <div class="widget-box2">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Isi Keterangan</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
          <form>
            <div class="controls">
              <textarea class="textarea_editor span12" rows="6"></textarea>
            </div>
        <div class="control-group">
          <label class="control-label">Dikirim Kepada</label>
            <div class="controls">
              <select name="jenis_user" id="jenis_user">
                <option value="1">Kepala BAPPEDA</option>
                <option value="2">Admin Surat</option>
                <option value="3">Sekretaris BAPPEDA</option>
                <option value="4">Kasubbag Umum</option>
                <option value="5">Bidang Keuangan</option>
                <option value="6">Bidang Pengembangan Permukiman dan Prasarana Wilayah (P3W)</option>
                <option value="7">Bidang Kesejahteraan Sosial dan Sumber Daya (KS2D)</option>
                <option value="8">Bidang Perekonomian dan Pemerintahan (PP)</option>
                <option value="9">Bagian Sekretariat</option>
              </select>
              </div>
            </div>
          </form>
            <div class="row-fluid">
              <a href="lihatsurat.php" class="btn"><i class="icon-print">  Cetak</i></a> 
              <a href="#" class="btn btn-success"><i class="icon-download-alt"> Unduh</i></a>
            </div>
        </div>
      </div>
    </div>
  </div>
          <?php }?>
          </div>
          </div>
        </div>
      </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>

<!--end-Footer-part-->



<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
