<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
	<meta charset="utf-8">
	
	<?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>



<?php
  //$this->load->view('page/headerbar');
  ?>
<?php
  $this->load->view('page/headeradmin');
  ?>

    
<body>
<!-- END RIGHTBAR -->
<div id="content">
  <div class="container-fluid">
 <div class="row-fluid">
    <?php
  		$this->load->view('page/sidebaruser');
  	?>
<div class="span9">
  
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>MANAGE USER</h5>
      </div>
      <div class="widget-content">
        <div class="control-group">
        	<div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Number</th>
                  <th>Full Name</th>
                  <th>Username</th>
                  <th>Address</th>
                  <th>Kind of User</th>
                  <th>E-Mail</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0;foreach ($pengguna as $data_pengguna) {$i++;?>
                <tr class="odd gradeX">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $data_pengguna->nama; ?></td>
				          <td class="center"> <?php echo $data_pengguna->username; ?></td>
                  <td class="center"> <?php echo $data_pengguna->alamat; ?></td>
				     <td class="center"> 
					  <?php
						if ($data_pengguna->jenis_user > 1) {
						echo "Teacher"; 
						} 
						else{
              echo "Student";
            }
						?>
				   </td>

				   <td class="center"> <?php echo $data_pengguna->email; ?></td>
                  
                  <td class="center">
                    <a
                    data-id="<?php echo $data_pengguna->id_pengguna; ?>"
                    data-nama="<?php echo $data_pengguna->nama; ?>"
                    data-username="<?php echo $data_pengguna->username; ?>"
                    data-alamat="<?php echo $data_pengguna->alamat; ?>"
                    data-jenis_user="<?php echo $data_pengguna->jenis_user; ?>"
                    data-email="<?php echo $data_pengguna->email; ?>"
                    <td class="taskOptions"><a href="<?php echo site_url('pengguna/editAdmin?id=' .$data_pengguna->id_pengguna) ?>" class="tip-top" title="Update&#013;Change your identity If you need"><i class="icon-edit"></i></a> <a href="<?php echo site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->id_pengguna) ?>" class="tip-top" title="Delete&#013;Can Delete all of data for one row"><i class="icon-remove"></i></a></td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
          </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; E-Learning</div>
</div>
<!--end-Footer-part-->

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
<!--
<script>
document.getElementById('hapus').onClick=function(){
  alert('Yakin Dihapus?');
    window.location="<?php echo site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->id_pengguna) ?>";

};
</script>
-->
</body>
</html>