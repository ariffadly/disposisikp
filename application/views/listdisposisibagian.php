<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebarbagian');
    ?>
<!-- START -->
  <div id="content">
  <div class="container-fluid">
  <h2>Disposisi Masuk</h2>
        <div class="widget-box2">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Daftar Disposisi Surat</h5>
          </div>
          <div class="widget-content nopadding">
             <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Tanggal Masuk</th>
                  <th>Tanggal Surat</th>
                  <th>Nomor Surat</th>
                  <th>Pengirim</th>
                  <th>Perihal</th>
                  <th>Disposisi Dari</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody align="center">
                <tr>
                  <td><center>27 September 2017</center></td>
                  <td><center>27 September 2017</center></td>
                  <td><center>20 September 2017</center></td>
                  <td><center>100/DINSOS/147/Sosial</center></td>
                  <td><center>Permintaan Tenaga Bantuan</center></td>
                  <td><center>27 September 2017</center></td>
                  <td><center>27 September 2017</center></td>
                  <td class="taskOptions">
                  <a href="lihatsurat.php" class="btn btn-mini">Lihat Surat</a> 
                  <!--<a href="#" class="btn btn-primary btn-mini">Disposisi</a> -->
                  <a href="#" class="btn btn-success btn-mini">Ekspedisi Surat</a></td>
                </tr>
                <tr>
                  <td><center>27 September 2017</center></td>
                  <td><center>27 September 2017</center></td>
                  <td><center>27 September 2017</center></td>
                  <td class="taskStatus"><span class="pending">pending</span></td>
                  <td><center>6 September 2017</center></td>
                  <td class="taskDesc"><i class="icon-info-sign"></i> Making The New Suit</td>
                  <td class="taskStatus"><span class="in-progress">in progress</span></td>
                  <td class="taskOptions">
                   <a href="lihatsurat.php" class="btn btn-mini">Lihat Surat</a>
                  <!--<a href="#" class="btn btn-primary btn-mini">Disposisi</a> -->
                  <a href="#" class="btn btn-success btn-mini">Ekspedisi Surat</a></td>
                </tr>
                <tr>
                  <td class="taskStatus"><span class="done">done</span></td>
                  <td class="taskStatus"><span class="done">done</span></td>
                  <td class="taskStatus"><span class="done">done</span></td>
                  <td class="taskStatus"><span class="done">done</span></td>
                  <td><center>31 Agustus 2017</center></td>
                  <td class="taskDesc"><i class="icon-info-sign"></i> Making The New Suit</td>
                  <td class="taskStatus"><span class="in-progress">in progress</span></td>
                  <td class="taskOptions">
                  <a href="<?php echo base_url()."pengguna/lihat_suratbagian/"; ?>" class="btn btn-mini">Lihat Surat</a> 
                  <!--<a href="#" class="btn btn-primary btn-mini">Disposisi</a> -->
                  <a href="#" class="btn btn-success btn-mini">Ekspedisi Surat</a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
</div>
      
        </div>
      </div>
    </div>
  </div>
</div>

<!--end-main-container-part-->


<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>

<!--end-Footer-part-->



<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
