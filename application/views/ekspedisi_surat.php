<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('page/title');
?>
<head>
    <meta charset="utf-8">

    <?php
    $this->load->view('page/meta_css');
    $this->load->view('page/js');

    ?>
</head>


<body>
<?php
$this->load->view('page/headeradmin');
?>
<!-- END RIGHTBAR -->

<?php
$this->load->view('page/sidebaruser');
?>
<!-- START -->
<div id="content">
    <div class="container-fluid" >
        <h2>Lacak Surat Masuk</h2>
<table>
    <?php
    $first = true;
    $i = 0;
    foreach ($ekspedisi as $item) {
        $cek = false;
        $a = $i == 0 ? $i : $i - 1;
    if($item->jenis_user_pengirim != $ekspedisi[$a]->jenis_user_pengirim || $first) {    ?>
    <tr>
        <td>
            <div class="tt" data-tt-id="<?=  $item->jenis_user_pengirim ?>" data-tt-parent="<?= $first ? '' : $ekspedisi[$a]->jenis_user_pengirim; ?>"><?= $item->jenis_user.' ('.$item->nama.') : '.$item->tanggal  ?> </div>
        </td>
    </tr>
    <?php }
        $k = $i+1;
        while (count($ekspedisi) > $k) {
            if ($item->jenis_user_target == $ekspedisi[$k]->jenis_user_pengirim) {
                $cek = true;
                $k = count($ekspedisi);
            }
            $k++;
        }

        if(!$cek) { ?>
            <tr>
                <td>
                    <div class="tt" data-tt-id="<?=  $item->jenis_user_target?>" data-tt-parent="<?=  $item->jenis_user_pengirim; ?>"><?= $item->jenis_target ?> </div>
                </td>
            </tr>
       <?php }
        if ($first) $first = false; $i++; } ?>
</table>
    </div></div>
</body>
</html>