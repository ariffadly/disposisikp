<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('page/title');
?>
<head>
    <meta charset="utf-8">

    <?php
    $this->load->view('page/meta_css');
    $this->load->view('page/js');

    ?>
</head>


<body>
<?php
$this->load->view('page/headeradmin');
?>
<!-- END RIGHTBAR -->

<?php
$this->load->view('page/sidebaruser');
?>
<!-- START -->
<div id="content">
    <div class="container-fluid">
        <h2>Masukkan Nomor Surat Keluar</h2>
        <div class="row-fluid">
            <div class="span8">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Arsip Penomoran Surat Keluar</h5>
                    </div>
                    <form action="<?php echo base_url().'suratkeluar/tambah_surat'; ?>" method='post' enctype="multipart/form-data" class="form-horizontal">
                        <div class="widget-content nopadding">
                            <div class="control-group">
                                <label class="control-label" style="padding-top: 10px; padding-left: 100px; padding-bottom:8px; !important;">Kode Pemohon Surat*</label>
                                <select style="margin-left: 10px; margin-top:6px; !important;" name="jenis_user" id="jenis_user"  required>
                                    <?php foreach ($jenis_user as $data) {
                                        if($data->jenis_user <> 2) { ?>
                                            <option value="<?= $data->jenis_user ?>"><?= $data->singkatan ?> </option>
                                        <?php }} ?>
                                </select>
                            </div>
                            <div class="control-group">
                            <label class="control-label" style="padding-top: 20px; padding-left: 32px; !important;">Kode Surat*</label>
                            <div class="controls controls-row">
                                <div class="controls-group">
                                    <input type="text" style="margin-left: 78px; !important;" placeholder="Kode Surat (Required)" id="kode_surat" name="kode_surat" onblur="setCode()" required>
                                </div>
                            </div>
                                <div class="controls-group">
                                    <label class="control-label" style="padding-top: 10px; padding-left: 95px; !important;">Nama Pemohon Surat</label>
                                    <div class="controls">
                                        <input style="margin-left: 15px; !important;" type="text" name="nama" placeholder="nama (optional)">
                                    </div>
                                </div>
                        <div class="controls-row">

                            <div class="controls-group">
                                <label class="control-label" style="padding-top: 10px; padding-left: 25px; !important;">Keterangan</label>
                                <div class="controls">
                                    <input type="text" style="margin-left: 84px; !important;" placeholder="Keterangan (optional)" name="keterangan">
                                </div>
                            </div>
                        </div>

                            <div class="controls-group">
                                <label class="control-label" style="padding-top: 10px; padding-left: 2px; !important;">Surat ke</label>
                                <div class="controls">
                                    <input type="number" style="margin-left: 88px; !important;" disabled name="id" value="<?= $last_id[0]->id +1 ?>"/>
                                </div>
                            </div>
                            <div class="controls-group">
                                <label class="control-label" style="padding-top: 10px; margin-left: 32px; !important;">Nomor Surat</label>
                                <div class="controls">
                                    <input style="margin-left: 76px;" type="text" name="no_surat" id="no_surat" readonly/>
                                </div>
                            </div>
                                <div class="control-group">
                                    <label class="control-label" style="padding-top: 15px; margin-left: 62px; !important;">Tanggal Pemohon</label>
                                    <div class="controls">
                                        <input type="date" style="margin-top: 4px; margin-left: 45px; !important;" placeholder="Tanggal Pemohon" id="tanggal" name="tanggal" required/>
                                    </div>
                                </div>
                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
</div>
</div>
</div>
</div>

<!--end-main-container-part-->


<!--Footer-part-->
<?php
$this->load->view('page/footer');
?>

<!--end-Footer-part-->
<!--end-Footer-part-->

<script type="text/javascript">
    $( function() {
        var kode_surat = new Array();
        <?php foreach($kode_surat as $kode){ ?>
        kode_surat.push('<?php echo $kode->nomor.' - '.$kode->keterangan; ?>');
        <?php } ?>
        $( "#kode_surat" ).autocomplete({
            source: kode_surat
        });
    } );

    function setCode() {
        var code = $("#kode_surat").val();
        var id = <?= $last_id[0]->id + 1 ?>;
        var jenis_user = $('#jenis_user').find(":selected").text();
        $("#no_surat").val(code.substr(0, code.indexOf(' ')) + '/Bappeda/' + id + '/' + jenis_user);
    }
</script>
<!--
<style>
    .ui-autocomplete {
        position: absolute;
        top: 0;
        left: 0;
        cursor: default;
    }
    .ui-menu {
        list-style: none;
        padding: 0;
        margin: 0;
        display: block;
        outline: 0;
    }
    .ui-menu .ui-menu {
        position: absolute;
    }
    .ui-menu .ui-menu-item {
        margin: 0;
        cursor: pointer;
        /* support: IE10, see #8844 */
        list-style-image: url("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7");
    }
    .ui-menu .ui-menu-item-wrapper {
        position: relative;
        padding: 3px 1em 3px .4em;
    }
    .ui-menu .ui-menu-divider {
        margin: 5px 0;
        height: 0;
        font-size: 0;
        line-height: 0;
        border-width: 1px 0 0 0;
    }
    .ui-menu .ui-state-focus,
    .ui-menu .ui-state-active {
        margin: -1px;
    }
    .ui-widget-content {
        border: 1px solid #dddddd;
        background: #ffffff;
        color: #333333;
    }

    /* Corner radius */
    .ui-corner-all,
    .ui-corner-top,
    .ui-corner-left,
    .ui-corner-tl {
        border-top-left-radius: 3px;
    }
    .ui-corner-all,
    .ui-corner-top,
    .ui-corner-right,
    .ui-corner-tr {
        border-top-right-radius: 3px;
    }
    .ui-corner-all,
    .ui-corner-bottom,
    .ui-corner-left,
    .ui-corner-bl {
        border-bottom-left-radius: 3px;
    }
    .ui-corner-all,
    .ui-corner-bottom,
    .ui-corner-right,
    .ui-corner-br {
        border-bottom-right-radius: 3px;
    }

    /* Overlays */
    .ui-widget-overlay {
        background: #aaaaaa;
        opacity: .3;
        filter: Alpha(Opacity=30); /* support: IE8 */
    }
    .ui-widget-shadow {
        -webkit-box-shadow: 0px 0px 5px #666666;
        box-shadow: 0px 0px 5px #666666;
    }
</style>
-->

</body>
</html>
