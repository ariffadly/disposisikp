<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebaruser');
    ?>
<!-- START -->
  <div id="content">
  <div class="container-fluid">
  <h2>Masukkan Surat Masuk</h2>
       <div class="row-fluid">
       <div class="span9">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Halaman Disposisi</h5>
        </div>
         <form action="<?php echo base_url().'suratmasuk/tambah_surat'; ?>" method='post' enctype="multipart/form-data" class="form-horizontal">
        <div class="widget-content nopadding">
          <div class="control-group">
              <label class="control-label" style="padding-top: 10px; padding-left: 80px; padding-bottom:8px; !important;">Surat Dari</label>
              <div class="controls">
                <input type="text" style="margin-left: 50px; !important;" id="pengirim" name="pengirim" placeholder="Pengirim Surat" required/>
              </div>
          </div>
            <div class="control-group">
          <label class="control-label" style="padding-top: 10px; padding-left: 100px; !important;">Tanggal Surat</label>
          <div class="controls controls-row">
          <div class="controls-group">
            <input type="date" style="margin-left: 29px; !important;" placeholder="Tanggal Surat" id="tanggal_surat" name="tanggal_surat" required/>
          </div>
          </div>
          <div class="controls-group">
          <label class="control-label" style="padding-top: 10px; padding-left: 93px; !important;">Nomor Surat</label>
             <input type="text" style="margin-top: 8px; margin-left: 35px; !important;" placeholder="Nomor Surat" id="nomor_surat" name="nomor_surat" required/>
          </div>
          <div class="control-group">
            <label class="control-label" style="padding-top: 15px; padding-left: 118px; !important;">Tanggal Diterima</label>
          <div class="controls">
            <input type="date" style="margin-top: 4px; margin-left: 9px; !important;" placeholder="Tanggal Terima" id="tanggal_diterima" name="tanggal_diterima" required/>
          </div>
          </div>
          <div class="control-group">
            <label class="control-label" style="padding-top: 15px; padding-left: 90px; !important;">Perihal Surat</label>
          <div class="controls">
            <textarea style="margin-left: 36px; !important;" placeholder="Perihal Surat" id="perihal_surat" name="perihal_surat" required/></textarea>
          </div>
          </div>

          <div class="control-group">
              <label class="control-label" style="padding-top: 15px; padding-left: 104px; !important;">Dikirim Kepada</label>
              <div class="controls" style="padding-top: 15px; padding-left: 104px; !important;">
                <select name="jenis_user" id="jenis_user">
                <?php foreach ($jenis_user as $data) {
                  if($data->jenis_user == 3 || $data->jenis_user == 4) { ?>
                  <option value="<?= $data->jenis_user ?>"><?= $data->nama ?> </option>
                  <?php }} ?>
                </select>
              </div>
            </div>
         <div class="control-group">
            <label class="control-label" style="padding-top: 15px; padding-left: 106px; !important;">Masukkan Surat</label>
            <div class="controls">
             <input style="margin-left: 18px; !important;" type="file" name="file" id="file" accept="application/pdf" required/>
                <h6><p class="p" style="margin-left: 102px; !important;">Masukkan dalam Format .PDF</p></h6>
           </div>
          </div>
             
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Kirim</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
      
        </div>
      </div>
    </div>
  </div>
</div>

<!--end-main-container-part-->


<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>

<!--end-Footer-part-->



<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
