<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('page/title');
?>
<head>
	<meta charset="utf-8">
	

	<?php
  $this->load->view('page/meta_css');
  ?>
  <link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>assets/plugins/datatables/dataTables.css' /> 

</head>

<body class=" ">

  <?php
  $this->load->view('page/headerbar');
  ?>
  <?php
  $this->load->view('page/header');
  ?>



  <div id="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php
    $this->load->view('page/sidebar');
    ?>

    <!-- BEGIN RIGHTBAR -->
    <?php
    $this->load->view('page/rigthbar');
    ?>

    
  </div>
</div>
<!-- END RIGHTBAR -->
<div id="page-content">
	<div id='wrap'>
		<!-- <div id="page-heading">
      <ol class="breadcrumb">
        <li><a href="index.htm">Dashboard</a></li>
        <li>Extras</li>
      </ol>
    
      <h1>Blank</h1>
      <div class="options">
            <div class="btn-toolbar">
              <div class="btn-group hidden-xs">
                <a href='#' class="btn btn-default dropdown-toggle" data-toggle='dropdown'><i class="fa fa-cloud-download"></i><span class="hidden-sm"> Export as  </span><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Text File (*.txt)</a></li>
                  <li><a href="#">Excel File (*.xlsx)</a></li>
                  <li><a href="#">PDF File (*.pdf)</a></li>
                </ul>
              </div>
              <a href="#" class="btn btn-default"><i class="fa fa-cog"></i></a>
            </div>
          </div>
        </div> -->
        <div class="container">



         <div class="row">
          <div class="col-md-12">
           <div class="panel panel-primary">
            <div class="panel-heading">
             <h4>Pengguna</h4>
             <div class="options">

             </div>
           </div>
           <div class="panel-body">
             <div class="col-md-12">
               <a class="btn btn-default" data-id='0' data-toggle="modal" data-target="#tambah-data"><i class="fa fa-plus"></i> <span>Tambah Data</span></a><br><br>
             </i></button>
           </div>
           <div class="col-md-12">

             <table class="table table-bordered table-striped" id="datatable-editable">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
				  
                  <th>Password</th>
                  <th>Nama</th>
                  <th>Alamat</th>
				  
                  <th>Jenis User</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=0;foreach ($pengguna as $data_pengguna) {$i++;?>
                <tr class="odd gradeX">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $data_pengguna->username; ?></td>
				  <td class="center"> <?php echo $data_pengguna->password; ?></td>
                  <td class="center"> <?php echo $data_pengguna->nama; ?></td>
				  
                  <td class="center"> <?php echo $data_pengguna->alamat; ?></td>
				     <td class="center"> 
					  <?php
						if ($data_pengguna->jenis_user > 1) {
						echo "Dosen"; 
						} 
						else {
							echo "Mahasiswa";
						}
						?>
						</td>
                  
                  <td class="center">
                    <a class="btn btn-warning btn-xs"
                    data-id="<?php echo $data_pengguna->id_pengguna; ?>"
                    data-username="<?php echo $data_pengguna->username; ?>"
                    data-password="<?php echo $data_pengguna->password; ?>"
                    data-nama="<?php echo $data_pengguna->nama; ?>"
                    data-alamat="<?php echo $data_pengguna->alamat; ?>"
                    data-jenis_user="<?php echo $data_pengguna->jenis_user; ?>"
                   data-toggle="modal" 
                    data-target="#edit-data">
                    <i class="fa fa-pencil"></i> <span>Edit</span></a>
                    &nbsp;
                    <a class="btn btn-danger btn-xs" href="javascript:;" data-id="<?php echo $data_pengguna->id_pengguna;; ?>" data-toggle="modal" data-target="#modal-hapus"><i class="fa fa-eraser"></i> <span>Delete</span></a></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>




          </div>
        </div>
      </div>
    </div>







  </div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->

<?php
$this->load->view('page/footer');
?>

</div> <!-- page-container -->

<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>!window.jQuery && document.write(unescape('%3Cscript src="assets/js/jquery-1.10.2.min.js"%3E%3C/script%3E'))</script>
<script type="text/javascript">!window.jQuery.ui && document.write(unescape('%3Cscript src="assets/js/jqueryui-1.10.3.min.js'))</script>
-->
<!-- ++++++++++++++++++++++++++++++ Untuk Hapus Data +++++++++++++++++++++++++++++++ -->
<!-- Modal Hpus -->
<!-- modal konfirmasi-->
<div id="modal-hapus" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-danger">
        <div class="panel-heading"> 
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
          <h4>Hapus Data</h4> 
        </div> 
        <div class="modal-body" style="">
          &nbsp;&nbsp;<i class="fa fa-warning"></i><strong> Apakah Anda yakin ingin menghapus data ini?</strong>
        </div>
        <div class="modal-footer">

          <a href="javascript:;" class="btn btn-danger" id="hapus-true">Ya</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <br><br>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- ++++++++++++++++++++++++++++++ Untuk Tambah Data +++++++++++++++++++++++++++++++ -->
<!-- Modal tambah data -->
<div id="tambah-data" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <form id="form-data" method="post" action="<?php echo base_url().'pengguna/tambah_pengguna'; ?>">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Data</h4>
        </div>

        <div class="modal-body">

          <fieldset>

            <div class="control-group">
              <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
                <div class="tiles-heading">
                  <div class="pull-left">Username</div>
                </div>
                <div class="tiles-body">
                  <input type="text" name="username" class="form-control" id="username">
                </div>
              </a>
            </div>
            <div class="control-group">
              <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
                <div class="tiles-heading">
                  <div class="pull-left">Password</div>
                </div>
                <div class="tiles-body">
                  <input type="password" name="password" class="form-control" id="password">
                </div>
              </a>
            </div>
            <div class="control-group">
              <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
                <div class="tiles-heading">
                  <div class="pull-left">Nama</div>
                </div>
                <div class="tiles-body">
                  <input type="text" name="nama" class="form-control" id="nama">
                </div>
              </a>
            </div>
            <div class="control-group">
              <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
                <div class="tiles-heading">
                  <div class="pull-left">Alamat</div>
                </div>
                <div class="tiles-body">
                  <input type="text" name="alamat" class="form-control" id="alamat">
                </div>
              </a>
            </div>
            <div class="control-group">
              <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
                <div class="tiles-heading">
                  <div class="pull-left">Jenis User</div>
                </div>
                <div class="tiles-body">
                  <select name="jenis_user" class="form-control" id="jenis_user">
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select>
                </div>
              </a>
            </div>

          </fieldset>


        </div>

        <div class="modal-footer">
          <button class="btn btn-info btn-submit"> Simpan</button>
          <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
        </div>

      </form>

    </div>
  </div>
</div>
<!-- ++++++++++++++++++++++++++++++ Untuk Edit Data +++++++++++++++++++++++++++++++ -->
<!-- Modal Edit data -->
<div id="edit-data" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <form id="form-data" method="post" action="<?php echo base_url().'pengguna/edit_pengguna'; ?>">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Data</h4>
        </div>

        <div class="modal-body">

          <fieldset>
           <input type="hidden" name="id" id="id" class="form-control">
           <div class="control-group">
            <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
              <div class="tiles-heading">
                <div class="pull-left">Username</div>
              </div>
              <div class="tiles-body">
                <input type="text" name="username" class="form-control" id="username">
              </div>
            </a>
          </div>
          <div class="control-group">
            <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
              <div class="tiles-heading">
                <div class="pull-left">Password</div>
              </div>
              <div class="tiles-body">
                <input type="password" name="password" class="form-control" id="password">
              </div>
            </a>
          </div>
          <div class="control-group">
            <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
              <div class="tiles-heading">
                <div class="pull-left">Nama</div>
              </div>
              <div class="tiles-body">
                <input type="text" name="nama" class="form-control" id="nama">
              </div>
            </a>
          </div>
		  <div class="control-group">
            <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
              <div class="tiles-heading">
                <div class="pull-left">Alamat</div>
              </div>
              <div class="tiles-body">
                <input type="text" name="alamat" class="form-control" id="alamat">
              </div>
            </a>
          </div>
          
          <div class="control-group">
            <a class="info-tiles tiles-inverse" href="#" style="margin-bottom: 0px;">
              <div class="tiles-heading">
                <div class="pull-left">Jenis User</div>
              </div>
              <div class="tiles-body">
                <input type="text" name="jenis_user" class="form-control" id="jenis_user">
              </div>
            </a>
          </div>

        </fieldset>


      </div>

      <div class="modal-footer">
        <button class="btn btn-info btn-submit"> Simpan</button>
        <button class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
      </div>

    </form>

  </div>
</div>
</div>
<?php
$this->load->view('page/js');
?>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/pluginss/jquery-datatables-editable/jquery.dataTables.js"></script> 
<script src="<?php echo base_url(); ?>assets/pluginss/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/pluginss/jquery-datatables-editable/datatables.pengguna.js"></script>
<script>

  $(function() {

    $('#modal-hapus').on('show.bs.modal', function(event) {
                                var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan

                                // Untuk mengambil nilai dari data-id="" yang telah kita tempatkan pada link hapus
                                var id = div.data('id')

                                var modal = $(this)

                                // Mengisi atribut href pada tombol ya yang kita berikan id hapus-true pada modal.
                                modal.find('#hapus-true').attr("href", "<?php echo base_url().'pengguna/hapus_pengguna/'; ?>" + id);
                              });
    $('#edit-data').on('show.bs.modal', function(event) {
                                var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
                                var id = div.data('id');
                                var username = div.data('username');
                                var password = div.data('password');
                                var nama = div.data('nama');
                                var alamat = div.data('alamat');
                                var jenis_user = div.data('jenis_user');
                                var modal = $(this);
                                
                                // Isi nilai pada field
                                modal.find('#id').attr("value", id);
                                modal.find('#username').attr("value", username);
                                modal.find('#password').attr("value", password);
                                modal.find('#nama').attr("value", nama);
								modal.find('#alamat').attr("value", alamat);
								
								modal.find('#jenis_user').attr("value", jenis_user);
                              });
  });
</script>
</body>
</html>