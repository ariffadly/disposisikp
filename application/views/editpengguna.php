<!DOCTYPE html>
<html lang="en">
<?php
$this->load->view('page/title');
?>
<head>
    <meta charset="utf-8">

    <?php
    $this->load->view('page/meta_css');
    $this->load->view('page/js');

    ?>
</head>


<body>
<?php
$this->load->view('page/headeradmin');
?>
<!-- END RIGHTBAR -->

<?php
$this->load->view('page/sidebaruser');
?>
<!-- START -->
<div id="content">
    <div class="container-fluid">
        <h2>Ubah Data Pengguna</h2>
        <div class="row-fluid">
            <div class="span9">
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2">
                        <h5>Ubah Data Diri</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form action="<?php echo base_url().'pengguna/editpengguna2'; ?>" method="post" class="form-horizontal">
                            <?php foreach ($pengguna as $data_pengguna){ ?>
                                <div class="control-group">
                                    <label class="control-label" style="margin-left: 40px; !important;">NIP</label>
                                    <div class="controls">
                                        <input type="text" style="margin-left: 93px; !important;" name="nip" id="nip" placeholder="Nomor Induk Pegawai" readonly="true" value="<?php echo $data_pengguna->nip;?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" style="margin-left: 111px; !important;">Nama Lengkap</label>
                                    <div class="controls">
                                        <input type="text" name="nama" id="nama" style="margin-left: 20px; !important;" placeholder="Nama Lengkap" value="<?php echo $data_pengguna->nama;?>">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" style="margin-left: 75px; !important;">Golongan</label>
                                    <div class="controls">
                                        <input type="text"  style="margin-left: 55px; !important;" name="golongan" id="golongan" placeholder="Golongan" value="<?php echo $data_pengguna->golongan;?>">
                                    </div>
                                </div>
                                <div class="control-group2">
                                    <label class="control-label" style="margin-left: 76px; !important;">Jenis User</label>
                                    <div class="controls">
                                        <select name="jenis_user" id="jenis_user" style="margin-left: 52px; !important;">
                                            <option value="1">Kepala BAPPEDA</option>
                                            <option value="2">Admin Surat</option>
                                            <option value="3">Sekretaris BAPPEDA</option>
                                            <option value="4">Kasubbag Umum</option>
                                            <option value="5">Bidang Keuangan</option>
                                            <option value="6">Bidang Pengembangan Permukiman dan Prasarana Wilayah (P3W)</option>
                                            <option value="7">Bidang Kesejahteraan Sosial dan Sumber Daya (KS2D)</option>
                                            <option value="8">Bidang Perekonomian dan Pemerintahan (PP)</option>
                                            <option value="9">Bagian Sekretariat</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="text-center" style="margin-top: 10px; !important;">
                                    <a class="tombol btn btn-default" title="Cancel" onclick="history.back()">Batal</a>
                                    <button class="tombol btn btn-info btn-submit" title="Save">Simpan</button>
                                </div>
                            <?php }
                            ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Footer-part-->
<?php
$this->load->view('page/footer');
?>
<!--end-Footer-part-->

<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>
<!--
<script>
document.getElementById('hapus').onClick=function(){
  alert('Yakin Dihapus?');
    window.location="<?php echo site_url('pengguna/hapus_pengguna?id=' .$data_pengguna->id_pengguna) ?>";

};
</script>
-->
</body>
</html>