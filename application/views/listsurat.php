<!DOCTYPE html>
<html lang="en">
<?php
  $this->load->view('page/title');
  ?>
<head>
  <meta charset="utf-8">
  
  <?php
  $this->load->view('page/meta_css');
  $this->load->view('page/js');
  
  ?>
</head>

    
<body>
<?php
  $this->load->view('page/headeradmin');
  ?>
<!-- END RIGHTBAR -->

    <?php
      $this->load->view('page/sidebaruser');
    ?>
<!-- START -->
  <div id="content">
  <div class="container-fluid">
  <h2>Daftar Surat Masuk</h2>
        <div class="widget-box2">
          <div class="widget-title"><span class="icon"><i class="icon-th"></i></span>
            <h5>Daftar Surat Masuk</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <!--<th>Nomor</th>-->
                  <th>Tanggal Masuk</th>
                  <th>Tanggal Surat</th>
                  <th>Nomor Surat</th>
                  <th>Pengirim</th>
                  <th style="width: 150px;">Perihal</th>
                  <th style="width: 120px; !important;">Aksi</th>
                </tr>
              </thead>
              <tbody align="center">
              <?php $i=0;foreach ($pengguna as $data_surat) {$i++;?>
                <tr class="odd gradeX"de>
                  <!--<td><?= $i; ?></td>-->
                  <td><?= $data_surat->tanggal_diterima; ?></td>
                  <td><?= $data_surat->tanggal_surat; ?></td>
                  <td><?= $data_surat->nomor_surat; ?></td>
                  <td><?= $data_surat->pengirim; ?></td>
                  <td><?= $data_surat->perihal_surat; ?></td>
                  

                   <td class="center">
                    <a
                    data-tglterima="<?= $data_surat->tanggal_diterima; ?>"
                    data-tglsurat="<?= $data_surat->tanggal_surat; ?>"
                    data-nmrsurat="<?= $data_surat->nomor_surat; ?>"
                    data-pengirim="<?= $data_surat->pengirim; ?>"
                    data-perihalsurat="<?= $data_surat->perihal_surat; ?>">

                        <center><a href="<?= base_url()."suratmasuk/halaman_lihatsurat/".$data_surat->id_surat; ?>" class="btn btn-mini">Lihat</a>
                  <!--<a href="#" class="btn btn-primary btn-mini">Disposisi</a>-->
                            <a href="<?= site_url('suratmasuk/ekspedisiSurat/'.$data_surat->id_surat) ?>" class="btn btn-info btn-mini" style="margin-left: 2px; !important;">Lacak</a>

                            <?php if($this->session->userdata('jenis_user') <> 2 && $this->session->userdata('jenis_user') <> $data_surat->jenis_user_pengirim) { ?>
                            <button type="button" class="btn btn-success btn-mini teruskan_surat" data-id="<?= $data_surat->id_surat ?>" data-toggle="modal" >Teruskan</a>
                       <?php } ?>
                   
                    </td>
                    </center>
                </tr>
                  <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
</div>
      
        </div>
      </div>
    </div>
  </div>
</div>

<!--end-main-container-part-->


<!--Footer-part-->
<?php
  $this->load->view('page/footer');
  ?>

<!--end-Footer-part-->



<!-- Teruskan Surat -->
<div id="teruskan_surat" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Teruskan Surat content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Diteruskan Ke</h4>
      </div>
      <form action="<?= site_url('suratmasuk/teruskan_surat'); ?>" method="post">
      <div class="modal-body">
      <div   required>
      <input type="hidden" name="id_surat" id="id_surat">
          <?php foreach ($jenis_user as $data) {
            if ($data->jenis_user <> $this->session->userdata('jenis_user') && $data->jenis_user <> 2) {
          ?>
          <label class="checkbox-inline" style="background-color: aliceblue; padding: 0.8em;" ><input type="checkbox" id="checkbox" name="jenis_user[]" value="<?= $data->jenis_user ?>" onclick="check()" required> <?= $data->nama ?></label>
          <?php }} ?>

          </div>

          <?php if($this->session->userdata('jenis_user') == 1) { ?>
          <textarea name="perintah" placeholder="Perintah... (cth: hadiri rapat di surat ini)" style="width: -webkit-fill-available; margin-top: 0.2em" required></textarea>
          <?php } ?>

      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-success" value="Kirim">
      </div>
      </form>
    </div>

  </div>
</div>

</body>

<script type="text/javascript">

function check() {
  $('form :input').removeAttr('required');
}


   $(".teruskan_surat").click(function(){
     $("#id_surat").val($(this).data('id'));
     $('#teruskan_surat').modal('show');
   });

//    $(".ekspedisi_surat").click(function(){
//     $("#id_surat").val($(this).data('id'));
//
////     $('#modalContent').html('Loading..');
//
//        $.ajax({
//            cache: false,
//            type: 'GET',
//            url: 'getEkspedisiSurat',
////            data: 'detail=' + id_beli,
//            success: function(data) {
//                console.log('masuk');
//                console.log(data);
//                $("#isi").append('<tr><td><div class="tt" data-tt-id="root" data-tt-parent="">Mark</div></td></tr>');
//                $('#ekspedisi_surat').modal('show');
//
//            }
//        });
//   });
</script>
</html>
