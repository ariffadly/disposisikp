<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Md_suratmasuk extends CI_Model
{

    function getSuratById($id)
    {
        $hasil = $this->db->from($this->suratmasuk)->where(['id_surat'=> $id])->order_by('tanggal_diterima','DESC')->get();
        $data = $hasil;
        return $data;
    }

    /*Untuk Surat*/
    function getSuratMasukAll($jenis_user)
    {
        if ($jenis_user == 2) {
            $this->db->from('suratmasuk')->order_by('tanggal_diterima','DESC');
            $query = $this->db->get();
            return $query->result();
        } else {
            $this->db->from('suratmasuk, ekspedisi_suratmasuk');
            $this->db->where(
                'suratmasuk.id_surat = ekspedisi_suratmasuk.id_suratmasuk AND ekspedisi_suratmasuk.jenis_user_target = ' . $jenis_user
            )->order_by('tanggal_diterima','DESC');

            $hasil = $this->db->get()->result();
        }
        return $hasil;
    }

    function getSuratMasuk($id_surat)
    {
        $hasil = $this->db->get_where('suratmasuk', ['id_surat' => $id_surat])->result();
        return $hasil;
    }

    function insertsurat($data_surat)
    {
        $this->db->insert('suratmasuk', $data_surat);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function insertEkspedisiSurat($data_ekspedisi)
    {
        if ($this->db->insert('ekspedisi_suratmasuk', $data_ekspedisi)) {
            return true;
        } else {
            return false;
        }
    }

    function updatePerintah($id_surat, $perintah)
    {

    }

    function getJenisUser($id) {
        return $this->db->get_where('jenis_user', ['jenis_user' => $id])->row()->nama;
    }

function getEkspedisi($id)
{
$this->db->select('pengguna.nip, pengguna.nama, jenis_user.nama jenis_user, ekspedisi_suratmasuk.*');
$this->db->from('ekspedisi_suratmasuk, jenis_user, pengguna, suratmasuk');
$this->db->where('ekspedisi_suratmasuk.id_suratmasuk = suratmasuk.id_surat 
AND ekspedisi_suratmasuk.id_pengguna = pengguna.nip 
AND pengguna.jenis_user = jenis_user.jenis_user AND ekspedisi_suratmasuk.id_suratmasuk = '.$id);
return $this->db->get()->result();
}
//Surat Selesai
}