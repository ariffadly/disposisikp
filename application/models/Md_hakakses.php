<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Md_hakakses extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getHakAkses(){
		$hasil =  $this->db->get('hak_akses')->result();
        return $hasil;
	}

}

/* End of file Md_hakakses */
/* Location: ./application/models/Md_hakakses */