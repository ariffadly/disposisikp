<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Md_suratkeluar extends CI_Model
{
    function getLastId() {
        $this->db->select('id');
        $this->db->from('suratkeluar');
        $this->db->order_by('id','DESC');
        $this->db->limit(1);
        $query=$this->db->get()->result();
        return $query ? $query : ['0'=>(object)['id' => 0, 'ket' => 'aa']];
    }

    function insertsurat($data_surat)
    {
        $this->db->insert('suratkeluar', $data_surat);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function getNoSuratKeluarAll() {
        $hasil = $this->db->get_where('daftar_nosurat')->result();
        return $hasil;
    }

    function getSuratKeluarAll() {
        $hasil = $this->db->get_where('suratkeluar, jenis_user', 'suratkeluar.id_jabatan_request = jenis_user.jenis_user')->result();
        return $hasil;
    }
}