<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Md_pengguna extends CI_Model {

    function getPenggunaAll() {
        $hasil = $this->db->get_where('pengguna')->result();
        return $hasil;
    }
    function insertPengguna($data_pengguna) {
        $hasil=$this->db->insert('pengguna', $data_pengguna);
        return $hasil;
    }

     function deletePengguna($id) {
        $hasil=$this->db->delete('pengguna', array('nip' => $id));
        return $hasil;
    }


    function getManagePengguna($id) {
        $this->db->order_by('nama','ASC');
        $hasil=$this->db->get_where('pengguna',array('id_pengguna !='=>$id,'jenis_user'=>1))->result();
        return $hasil;

    }
    
    function getPenggunaByNIP($nip) {
        $hasil=$this->db->get_where('pengguna',array('nip '=>$nip))->result();
        return $hasil;
    }
    function getPenggunaById($id) {
        $hasil=$this->db->get_where('pengguna',array('nip '=>$id))->result();
        $data = $hasil;
        return $data;
    }
    function updatePengguna($param, $data) {
        $this->db->where('nip', $param);
        $this->db->update('pengguna', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getJenisUserAll() {
        $hasil = $this->db->get_where('jenis_user')->result();
        return $hasil;
    }
    function updateProfile($param, $data) {
        $this->db->where('nip', $param);
        $this->db->update('pengguna', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
}